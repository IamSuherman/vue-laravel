@extends('layouts.app')

@section('content')

<div class="container">
    <div class="container-fluid">
        <div class="jumbotron"  style="background: #EC6F66;  /* fallback for old browsers */background: -webkit-linear-gradient(to right, #F3A183, #EC6F66);  /* Chrome 10-25, Safari 5.1-6 */background: linear-gradient(to right, #F3A183, #EC6F66); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */" width=100% height=100%>
        <div class="image">        
            <h1 class="display-4 animated infinite bounce delay-1s" style="animation-duration: 3s;">Hello, Guys! </h1>
            <p class="lead animated fadeInRight	">This is my simple website</p><i class="fas fa-allergies"></i>
            <hr class="my-4">
            <router-link to="/" class="btn btn-primary btn-sm hvr-float-shadow">Home</router-link>
            <router-link to="/create" class="btn btn-danger btn-sm hvr-float-shadow">Create</router-link><hr>      
        <!-- route outlet -->
        <!-- component matched by the route will render here -->
        <router-view></router-view>
        </div>
        </div>
    </div>
</div>

@endsection
